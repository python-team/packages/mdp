Source: mdp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Tiziano Zito <opossumnano@gmail.com>,
           Yaroslav Halchenko <debian@onerussian.com>
Testsuite: autopkgtest-pkg-python
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-pytest,
               python3-numpy,
               python3-scipy,
               python3-joblib,
               python3-sklearn,
               python3-libsvm,
               python3-zombie-imp,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/mdp
Vcs-Git: https://salsa.debian.org/python-team/packages/mdp.git
Homepage: https://mdp-toolkit.github.io/
Rules-Requires-Root: no

Package: python3-mdp
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-numpy
Recommends: python3-pytest,
            python3-scipy,
            python3-joblib,
            python3-sklearn,
            python3-libsvm,
Description: Modular toolkit for Data Processing
 Python data processing framework for building complex data processing software
 by combining widely used machine learning algorithms into pipelines and
 networks. Implemented algorithms include: Principal Component Analysis (PCA),
 Independent Component Analysis (ICA), Slow Feature Analysis (SFA), Independent
 Slow Feature Analysis (ISFA), Growing Neural Gas (GNG), Factor Analysis,
 Fisher Discriminant Analysis (FDA), and Gaussian Classifiers.
